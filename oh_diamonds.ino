#define BAUD_RATE 115200
#define N 4
#define M 3

byte pins[N][M] = {
	{2, 7, 10},
	{5, 12, 8},
	{6, 11, 3},
	{13, 9, 4},
};

void setup() {
	Serial.begin(BAUD_RATE);
	for (byte pin = 2; pin <= 13; ++pin) {
		pinMode(pin, OUTPUT);
		digitalWrite(pin, LOW);
	}
}

void loop() {
	char c;
	byte n;
	while (Serial.available() > 0) {
		c = Serial.read();
		switch (c) {
			case 'R': n = 0; break;
			case 'G': n = 1; break;
			case 'B': n = 2; break;
			case 'W': n = 3; break;
			default: n = 255; break;
		}
		if (n < N) {
			for (byte m = 0; m < M; ++m) {
				if (digitalRead(pins[n][m]) == LOW) {
					digitalWrite(pins[n][m], HIGH);
					break;
				}
			}
		}
	}
}
