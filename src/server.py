import bottle
import serial

SERIAL_BAUD_RATE = 115200
SERIAL_COM_PORT = 'COM6'

stories = {
	'wedding':	b'R',
	'birth':	b'G',
	'travel':	b'B',
	'career':	b'W',
}


@bottle.get('/static/<filename>')
def static(filename):
	return bottle.static_file(filename, root='static/')


with serial.Serial(SERIAL_COM_PORT, SERIAL_BAUD_RATE) as com:
	@bottle.route('/', method=('POST', 'GET'))
	def index():
		story = bottle.request.query.get('story')
		if story in stories:
			com.write(stories[story])
			resp = bottle.static_file('done.html', root='src/')
			resp.set_header('Cache-Control', 'no-store')
			return resp
		else:
			return bottle.static_file('form.html', root='src/')

	bottle.run(host='0.0.0.0', port=8080)
